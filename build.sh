#!/bin/bash

CURRENT_PATH=$(cd -- $(dirname $0) >/dev/null 2>&1 ; pwd -P)

if [[ "$OSTYPE" =~ ^msys ]]; then
    CURRENT_PATH="${CURRENT_PATH:0:2}:${CURRENT_PATH:2}"
fi

source ./venv/Scripts/activate

BUILD_DIR=build

pyinstaller \
    --onefile \
    --console \
    --log-level=WARN \
    --collect-all dateutil \
    --add-data "$CURRENT_PATH\\venv\\Lib\\site-packages\\yaspin\\data;yaspin\\data" \
    --distpath "./$BUILD_DIR" \
    src/main.py

ENV_FILE=.env.example

if [ ! -f "./$BUILD_DIR/$ENV_FILE" ] || [ "./$ENV_FILE" -nt "./$BUILD_DIR/$ENV_FILE" ]; then
    cp ./$ENV_FILE ./$BUILD_DIR/$ENV_FILE
fi

TEMPLATE_DIR=./$BUILD_DIR/storage/templates

if [ ! -d $TEMPLATE_DIR  ]; then
    mkdir -p $TEMPLATE_DIR
fi

DOC_MONITORING=./src/storage/templates/monitoring_template.docx
DOC_LOADING=./src/storage/templates/loading_template.docx

if test -f "$DOC_MONITORING"; then
    cp $DOC_MONITORING $TEMPLATE_DIR
else
    echo "File not found and not copied: $DOC_MONITORING"
fi

if test -f "$DOC_LOADING"; then
    cp $DOC_LOADING $TEMPLATE_DIR
else
    echo "File not found and not copied: $DOC_LOADING"
fi
