import arrow

from utils.helpers import dictValue
from utils.grafana.main import Grafana

def uploadInfo(fromDate, toDate) -> dict:
    if type(fromDate) != arrow.arrow.Arrow:
        raise Exception('Invalid parameter: fromDate.')

    if type(toDate) != arrow.arrow.Arrow:
        raise Exception('Invalid parameter: toDate.')

    _result = {}

    eltStatus = Grafana.eltDeltaStatus(fromDate, toDate)

    if eltStatus is None:
        return _result

    eltStatusSeries = dictValue(eltStatus, 'data.results.0.series')

    if eltStatusSeries is None:
        return _result

    for status in eltStatusSeries:
        _name = dictValue(status, 'tags.query')
        _value = dictValue(status, 'values')

        _result[_name] = _value[-1][1]

    return _result

def uploadInfoDiff(fromDate, toDate, isDaily) -> dict:
    if type(fromDate) != arrow.arrow.Arrow:
        raise Exception('Invalid parameter: fromDate.')

    if type(toDate) != arrow.arrow.Arrow:
        raise Exception('Invalid parameter: toDate.')

    lastDay = None
    firstDay = None

    if isDaily:
        lastDay = uploadInfo(fromDate, toDate)
        firstDay = uploadInfo(fromDate.shift(days=-1), toDate.shift(days=-1))
    else:
        lastDay = uploadInfo(toDate.floor('day'), toDate.ceil('day'))
        firstDay = uploadInfo(fromDate.floor('day'), fromDate.ceil('day'))

    keys = [
        'indicators_metadescription',
        'measurements',
        'characteristics',
        'dictionary_indicators_list',
        'okogu',
        'section_subsection',
        'national_project',
        'fpsr_dictionary_list',
        'program_subprogram',
    ]

    _result = {}

    for key in keys:
        if key in lastDay and key in firstDay:
            _result[key] = lastDay[key] - firstDay[key]

    return _result
