import arrow
import traceback
from os import path
from win32com import client

from config.env import env
from utils.spin import spin
from config.env import storagePath
from utils.helpers import stacktrace, monthName
from docs.loading.meta import uploadInfo, uploadInfoDiff

"""
BOOKMARKS

Tables:
 - table_upload_info : Таблица 2
 - table_upload_diff_info : Таблица 3
"""

def __fillInTemplate(document, fromDate, toDate, isDaily):
    docTables = {
        'table_upload_info': None,
        'table_upload_diff_info': None,
    }

    for table in document.Tables:
        firstCell = table.Cell(1, 1).Range

        for bookmark in docTables.keys():
            if firstCell.Bookmarks.Exists(bookmark):
                docTables[bookmark] = table

    if any(l is None for l in docTables.values()):
        spin.fail("Не верный шаблон регламентной загрузки. Процесс остановлен.")

        return None

    spin.start('Получение данных количество объектов, выгруженных из ГИС ЦАП. . .')

    theTable = docTables.get('table_upload_info')
    table2Data = uploadInfo(fromDate, toDate)

    spin.update('Заполнение Таблицы 2 - \'Перечень и общее количество объектов, выгруженных из ГИС ЦАП\'. . .')

    theTable.Cell(2, 3).Range.Text = table2Data.get('indicators_metadescription', '?')
    theTable.Cell(3, 3).Range.Text = table2Data.get('measurements', '?')
    theTable.Cell(4, 3).Range.Text = table2Data.get('characteristics', '?')
    theTable.Cell(5, 3).Range.Text = table2Data.get('dictionary_indicators_list', '?')
    theTable.Cell(6, 3).Range.Text = table2Data.get('okogu', '?')
    theTable.Cell(7, 3).Range.Text = table2Data.get('section_subsection', '?')
    theTable.Cell(8, 3).Range.Text = table2Data.get('national_project', '?')
    theTable.Cell(9, 3).Range.Text = table2Data.get('fpsr_dictionary_list', '?')
    theTable.Cell(10, 3).Range.Text = table2Data.get('program_subprogram', '?')

    spin.ok('Таблица 2 - заполнена \'Перечень и общее количество объектов, выгруженных из ГИС ЦАП\'')

    spin.start('Получение результатов регламентной загрузки в ВД, выгруженных из ГИС ЦАП. . .')

    theTable = docTables.get('table_upload_diff_info')
    table3Data = uploadInfoDiff(fromDate, toDate, isDaily)

    spin.update('Заполнение Таблицы 3 - \'Результаты регламентной загрузки в ВД, выгруженных из ГИС ЦАП\'. . .')

    theTable.Cell(2, 3).Range.Text = table3Data.get('indicators_metadescription', '?')
    theTable.Cell(3, 3).Range.Text = table3Data.get('measurements', '?')
    theTable.Cell(4, 3).Range.Text = table3Data.get('characteristics', '?')
    theTable.Cell(5, 3).Range.Text = table3Data.get('dictionary_indicators_list', '?')
    theTable.Cell(6, 3).Range.Text = table3Data.get('okogu', '?')
    theTable.Cell(7, 3).Range.Text = table3Data.get('section_subsection', '?')
    theTable.Cell(8, 3).Range.Text = table3Data.get('national_project', '?')
    theTable.Cell(9, 3).Range.Text = table3Data.get('fpsr_dictionary_list', '?')
    theTable.Cell(10, 3).Range.Text = table3Data.get('program_subprogram', '?')

    theTable.Cell(2, 5).Range.Text = f"{table3Data.get('indicators_metadescription', '?')} / 0"
    theTable.Cell(3, 5).Range.Text = f"{table3Data.get('measurements', '?')} / 0"
    theTable.Cell(4, 5).Range.Text = f"{table3Data.get('characteristics', '?')} / 0"
    theTable.Cell(5, 5).Range.Text = f"{table3Data.get('dictionary_indicators_list', '?')} / 0"
    theTable.Cell(6, 5).Range.Text = f"{table3Data.get('okogu', '?')} / 0"

    spin.ok('Таблица 3 - заполнена \'Результаты регламентной загрузки в ВД, выгруженных из ГИС ЦАП\'')

    return document

def main(fromDate, toDate, isDaily) -> bool:
    if type(fromDate) != arrow.arrow.Arrow:
        raise Exception('Invalid parameter: fromDate.')

    if type(toDate) != arrow.arrow.Arrow:
        raise Exception('Invalid parameter: toDate.')

    templatePath = storagePath('./templates/loading_template.docx')

    if not path.exists(templatePath):
        print(f'FAIL Шаблон документа регламентной загрузки не обнаружен в {templatePath}')

        return False

    reportDate = fromDate.format("DD.MM.YYYY")

    if not isDaily:
        reportDate = monthName(fromDate) + ' ' + fromDate.format("YYYY")

    print(f'INFO Создание отчета регламентной загрузки за {reportDate}')

    spin.start('Подготовка к созданию отчета. . .')

    try:
        msWord = client.DispatchEx('Word.Application')
        msWord.Visible = 0

        msDocx = msWord.Documents.Open(templatePath)

        try:
            msDocx.CustomDocumentProperties('ReportDate').value = reportDate
            msDocx.CustomDocumentProperties('CurrentDate').value = arrow.now().format('DD.MM.YYYY')
            msDocx.Fields.Update()

            for l in range(0, msDocx.TablesOfContents.Count):
                msDocx.TablesOfContents(l + 1).Update()

            document = __fillInTemplate(msDocx, fromDate, toDate, isDaily)

            if document is None:
                return False

            spin.start('Сохранение отчета. . .')

            fileName = f'{env.LOAD_FILE_NAME} за {reportDate}.docx'
            saveAs = path.join(env.OUT_DIR, fileName)

            document.SaveAs(saveAs)

            spin.ok(f'Отчет сохранен как: {saveAs}')
        except Exception as error:
            print(f'MsW Error: {error}')

            stacktrace()

            return False
    except Exception as error:
        print(f'App Error: {error}')

        stacktrace()

        return False
    finally:
        spin.stop()

        msWord.ActiveDocument.Close(False)
        msWord.Quit(False)

    return True
