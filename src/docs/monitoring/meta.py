import arrow

from utils.grafana.main import Grafana
from utils.graylog.main import Graylog
from utils.helpers import dictValue, convertSize

def queryStatus(fromDate, toDate) -> dict:
    if type(fromDate) != arrow.arrow.Arrow:
        raise Exception('Invalid parameter: fromDate.')

    if type(toDate) != arrow.arrow.Arrow:
        raise Exception('Invalid parameter: toDate.')

    _result = {}

    queryStatus = Grafana.queryStatus(fromDate, toDate)
    queryStatusData = dictValue(queryStatus, 'data.results.B.frames.0.data.values')

    if queryStatusData is None or len(queryStatusData) == 0:
        return _result

    statusLen = len(queryStatusData[0])

    for l in range(statusLen):
        _consumer = queryStatusData[1][l]
        _rzMnemonic = queryStatusData[2][l]
        _successCount = queryStatusData[3][l]

        partsRzMnemonic = _rzMnemonic.split('.')

        if len(partsRzMnemonic) > 2:
            _rzMnemonic = partsRzMnemonic[1]

        if _rzMnemonic not in _result:
            _result[_rzMnemonic] = {}

        if _consumer not in _result[_rzMnemonic]:
            _result[_rzMnemonic][_consumer] = 0

        _result[_rzMnemonic][_consumer] += _successCount

    return _result

def serviceAccess(fromDate, toDate):
    if type(fromDate) != arrow.arrow.Arrow:
        raise Exception('Invalid parameter: fromDate.')

    if type(toDate) != arrow.arrow.Arrow:
        raise Exception('Invalid parameter: toDate.')

    _result = {}

    currentAlert = Grafana.serviceCurrentAlert(fromDate, toDate)
    globalAvailability = Grafana.serviceGlobalAvailability(fromDate, toDate)

    if currentAlert is not None:
        data = dictValue(currentAlert, 'data.results.0.series.0.values')

        if data:
            _result['alerts'] = data[-1][1]
        else:
            _result['alerts'] = 'Потерянные пакеты отсутствуют'

    if globalAvailability is not None:
        data = dictValue(globalAvailability, 'data.results.0.series.0.values')

        if data:
            _result['availability'] = f'{round(data[0][1], 2)}%'

    return _result

def telegrafMetrics(fromDate, toDate):
    if type(fromDate) != arrow.arrow.Arrow:
        raise Exception('Invalid parameter: fromDate.')

    if type(toDate) != arrow.arrow.Arrow:
        raise Exception('Invalid parameter: toDate.')

    _result = {}

    laMedium = Grafana.telegrafLaMedium(fromDate, toDate)
    zombies = Grafana.telegrafZombies(fromDate, toDate)
    processes = Grafana.telegrafProcesses(fromDate, toDate)
    threads = Grafana.telegrafThreads(fromDate, toDate)
    cpuUsage = Grafana.telegrafCpuUsage(fromDate, toDate)
    ramUsage = Grafana.telegrafRamUsage(fromDate, toDate)
    rootFsUsed = Grafana.telegrafRootFsUsed(fromDate, toDate)
    ioWait = Grafana.telegrafIoWait(fromDate, toDate)
    tcpConnections = Grafana.telegrafTcpConnections(fromDate, toDate)
    networkUsage = Grafana.telegrafNetworkUsage(fromDate, toDate)
    diskUsage = Grafana.telegrafDiskUsage(fromDate, toDate)
    swapUsage = Grafana.telegrafSwapUsage(fromDate, toDate)

    if laMedium is not None:
        data = dictValue(laMedium, 'data.results.0.series.0.values')

        if data:
            data = list(filter(lambda elem: elem[1] is not None, data))

            value = round(data[-1][1], 1)

            _result['la_medium'] = 'Потерянные пакеты отсутствуют' if value == 0 else str(value)

    if zombies is not None:
        data = dictValue(zombies, 'data.results.0.series.0.values')

        if data:
            data = list(filter(lambda elem: elem[1] is not None, data))

            _result['zombies'] = data[-1][1]

    if processes is not None:
        data = dictValue(processes, 'data.results.0.series.0.values')

        if data:
            data = list(filter(lambda elem: elem[1] is not None, data))

            _result['processes'] = data[-1][1]

    if threads is not None:
        data = dictValue(threads, 'data.results.0.series.0.values')

        if data:
            data = list(filter(lambda elem: elem[1] is not None, data))

            _result['threads'] = data[-1][1]

    if cpuUsage is not None:
        data = dictValue(cpuUsage, 'data.results.0.series.0.values')

        if data:
            data = list(filter(lambda elem: elem[1] is not None, data))

            _result['cpuUsage'] = f'{round(data[-1][1], 3)}%'

    if ramUsage is not None:
        data = dictValue(ramUsage, 'data.results.0.series.0.values')

        if data:
            data = list(filter(lambda elem: elem[1] is not None, data))

            _result['ramUsage'] = f'{round(data[-1][1], 1)}%'

    if rootFsUsed is not None:
        data = dictValue(rootFsUsed, 'data.results.0.series.0.values')

        if data:
            data = list(filter(lambda elem: elem[1] is not None, data))

            _result['rootFsUsed'] = f'{round(data[-1][1])}%'

    if ioWait is not None:
        data = dictValue(ioWait, 'data.results.0.series.0.values')

        if data:
            data = list(filter(lambda elem: elem[1] is not None, data))

            _result['ioWait'] = f'{round(data[-1][1], 1)}%'

    if tcpConnections is not None:
        data = dictValue(tcpConnections, 'data.results.0.series.0.values')

        if data:
            values = list(
                filter(
                    lambda elem: elem is not None,
                    map(lambda elem: elem[4], data)
                )
            )
            tcpMax = max(values)
            tcpAvg = sum(values) / len(data)

            _result['tcp_connections'] = f'Изменение max/average {round(tcpMax)}/{round(tcpAvg)}'

    if networkUsage is not None:
        data = dictValue(networkUsage, 'data')

        if data:
            networkIn = dictValue(data, 'results.0.series.0.values')
            networkOut = dictValue(data, 'results.1.series.0.values')

            if networkIn and networkOut:
                _result['network_usage'] = f'{round(networkIn[-1][1] / 1000)}/{round(networkOut[-1][1] / 1000)}'

    if diskUsage is not None:
        data = dictValue(diskUsage, 'data.results.0.series.0.values')

        if data:
            data = list(filter(lambda elem: elem[1] is not None, data))

            _result['disk_usage'] = convertSize(data[-1][1] - data[-1][2])

    if swapUsage is not None:
        data = dictValue(swapUsage, 'data.results.0.series.0.values')

        if data:
            data = list(filter(lambda elem: elem[1] is not None, data))

            _result['swap_usage'] = data[-1][1]

    return _result

def queryExecuteTime(fromDate, toDate) -> dict:
    if type(fromDate) != arrow.arrow.Arrow:
        raise Exception('Invalid parameter: fromDate.')

    if type(toDate) != arrow.arrow.Arrow:
        raise Exception('Invalid parameter: toDate.')

    _result = {}

    queryTime = Grafana.queryExecuteTime(fromDate, toDate)

    if queryTime is None:
        return _result

    queryTimeFields = dictValue(queryTime, 'data.results.A.frames.0.schema.fields')
    queryTimeValues = dictValue(queryTime, 'data.results.A.frames.0.data.values')

    for l, field in enumerate(queryTimeFields):
        fieldName = dictValue(field, 'labels.reg_query')

        if fieldName is None:
            continue

        values = list(filter(lambda elem: elem is not None, queryTimeValues[l]))
        valueAvg = sum(values) / len(values)

        if valueAvg < 1000:
            valueAvg = f'{round(valueAvg, 1)} ms'
        else:
            valueAvg = f'{round(valueAvg / 1000, 1)} s'

        _result[fieldName] = valueAvg

    return _result

def alertMetrics(fromDate, toDate):
    if type(fromDate) != arrow.arrow.Arrow:
        raise Exception('Invalid parameter: fromDate.')

    if type(toDate) != arrow.arrow.Arrow:
        raise Exception('Invalid parameter: toDate.')

    _result = {}

    metrics = Grafana.alertMetrics(fromDate, toDate)

    if metrics is not None:
        data = dictValue(metrics, 'data.results.A.frames.0.data.values')

        if data[1]:
            values = list(filter(lambda elem: elem is not None, data[1]))

            meanSpeed = sum(values) / len(values)
            _result['mean'] = f'{round(meanSpeed, 5)} req/s'

    return _result

def jvmMetrics(fromDate, toDate):
    if type(fromDate) != arrow.arrow.Arrow:
        raise Exception('Invalid parameter: fromDate.')

    if type(toDate) != arrow.arrow.Arrow:
        raise Exception('Invalid parameter: toDate.')

    _result = {}

    memoryUsage = Grafana.jvmMemoryUsage(fromDate, toDate)
    gcCollectionCount = Grafana.jvmGcCollectionCount(fromDate, toDate)
    gcCollectionTime = Grafana.jvmGcCollectionTime(fromDate, toDate)
    memoryPool = Grafana.jvmMemoryPool(fromDate, toDate)
    threadsCount = Grafana.jvmThreadsCount(fromDate, toDate)

    if memoryUsage is not None:
        data = dictValue(memoryUsage, 'data.results.0.series.0.values')

        if data:
            value = sum(map(lambda elem: elem[1], data)) / len(data)
            _result['memory_usage'] = convertSize(value)

    if gcCollectionCount is not None:
        data = dictValue(gcCollectionCount, 'data.results.0.series.1.values')

        if data:
            value = sum(map(lambda elem: elem[1], data))
            _result['collection_count'] = value

    if gcCollectionTime is not None:
        data = dictValue(gcCollectionTime, 'data.results.0.series.1.values')

        if data:
            value = max(map(lambda elem: elem[1], data))

            if value < 1000:
                value = f'{round(value, 1)} ms'
            else:
                value = f'{round(value / 1000, 1)} s'

            _result['collection_time'] = value

    if memoryPool is not None:
        data = dictValue(memoryPool, 'data.results.0.series.0.values')

        if data:
            value = sum(map(lambda elem: elem[1], data)) / len(data)
            _result['memory_pool'] = convertSize(value)

    if threadsCount is not None:
        data = dictValue(threadsCount, 'data.results.0.series.0.values')

        if data:
            data = list(filter(lambda elem: elem[1] is not None, data))
            _result['threads_count'] = str(round(data[-1][1], 1))

    return _result

def kafkaLag(fromDate, toDate):
    if type(fromDate) != arrow.arrow.Arrow:
        raise Exception('Invalid parameter: fromDate.')

    if type(toDate) != arrow.arrow.Arrow:
        raise Exception('Invalid parameter: toDate.')

    _result = {
        'STATINFO.IN': '?',
        'uploadToS3': '?',
    }

    kafkaLag = Grafana.eltDeltaKafkaLag(fromDate, toDate)

    if kafkaLag is None:
        return _result

    kafkaLagData = dictValue(kafkaLag, 'data.results.0.series')

    if kafkaLagData is None:
        return _result

    for lagData in kafkaLagData:
        _tagName = dictValue(lagData, 'tags.topic')
        _values = list(
            filter(
                lambda elem: elem is not None,
                map(lambda elem: elem[1], lagData['values'])
            )
        )

        _result[_tagName] = _values[-1] or 0

    return _result

def eventsStatus(fromDate, toDate):
    if type(fromDate) != arrow.arrow.Arrow:
        raise Exception('Invalid parameter: fromDate.')

    if type(toDate) != arrow.arrow.Arrow:
        raise Exception('Invalid parameter: toDate.')

    _result = {}

    value = Graylog.viewsSearchExecuteValue('INFO', fromDate, toDate)

    if value:
        _result['info'] = value

    value = Graylog.viewsSearchExecuteValue('DEBUG', fromDate, toDate)

    if value:
        _result['debug'] = value

    value = Graylog.viewsSearchExecuteValue('WARN, ERROR', fromDate, toDate)

    if value:
        _result['warn'] = value

    return _result
