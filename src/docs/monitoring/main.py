import arrow
import shutil
import traceback
from os import path
from win32com import client

from config.env import env
from utils.spin import spin
from config.env import storagePath
from utils.helpers import isExe, stacktrace, monthName
from docs.monitoring.image import webPagesLen, dashboardImages
from docs.monitoring.meta import (
    queryStatus,
    serviceAccess,
    telegrafMetrics,
    queryExecuteTime,
    alertMetrics,
    jvmMetrics,
    kafkaLag,
    eventsStatus,
)

"""
BOOKMARKS

Tables:
 - table_rz_status : Таблица 1
 - table_service_access : Таблица 2
 - table_server_access : Таблица 3
 - table_telegraf : Таблица 4
 - table_system_alert : Таблица 5
 - table_podd_query_status : Таблица 6
 - table_podd_agent_metrics : Таблица 7
 - table_jvm_metrics : Таблица 8
 - table_delta_processed : Таблица 9.1
 - table_events : Таблица 10

Images
 - image_global_status : Рисунок 1
 - image_service_access : Рисунок 2
 - image_telegraf : Рисунок 3
 - image_system_alert : Рисунок 4
 - image_podd_query_status : Рисунок 5
 - image_podd_agent_metrics : Рисунок 6
 - image_jvm_metrics : Рисунок 7
 - image_elt_delta : Рисунок 8
"""

# Mapping of IS mnemonic and its original name
MNEMONIC_NAMES = {
    'ECON19': 'ГИС «Экономика»',
    'MNSV85': 'ПИАО НСУД',
    'RKZN01_3S': 'ГАСУ',
    '45B101': 'BI система Координационного Центра РФ',
}

def __fillInTemplate(document, fromDate, toDate):
    docTables = {
        'table_rz_status': None,
        'table_service_access': None,
        'table_server_access': None,
        'table_telegraf': None,
        'table_system_alert': None,
        'table_podd_query_status': None,
        'table_podd_agent_metrics': None,
        'table_jvm_metrics': None,
        'table_delta_processed': None,
        'table_events': None,
    }

    for table in document.Tables:
        firstCell = table.Cell(1, 1).Range

        for bookmark in docTables.keys():
            if firstCell.Bookmarks.Exists(bookmark):
                docTables[bookmark] = table

    if any(l is None for l in docTables.values()):
        spin.fail("Не верный шаблон для мониторинга РЗ. Процесс остановлен.")

        return None

    spin.start('Получение перечня зафиксированных РЗ. . .')

    table1 = docTables.get('table_rz_status')
    table1Data = queryStatus(fromDate, toDate)

    spin.update('Заполнение Таблицы 1 (Перечень зафиксированных РЗ). . .')

    for rzMnemonic, mnemonics in table1Data.items():
        for rowIndex in range(2, table1.Rows.Count + 1):
            textContent = table1.Cell(rowIndex, 2).Range.Text

            if rzMnemonic not in textContent:
                continue

            offset = 0

            for mnemonic, amount in mnemonics.items():

                if offset > 0:
                    if rowIndex + offset < table1.Rows.Count:
                        table1.Rows.Add(table1.Rows[rowIndex + offset - 1])
                    else:
                        table1.Rows.Add()

                _isName = MNEMONIC_NAMES[mnemonic] if mnemonic in MNEMONIC_NAMES else mnemonic

                table1.Cell(rowIndex + offset, 3).Range.Text = _isName
                table1.Cell(rowIndex + offset, 4).Range.Text = amount
                table1.Cell(rowIndex + offset, 5).Range.Text = amount

                offset += 1

            break

    spin.ok(f'Таблица 1 заполнена (Перечень зафиксированных РЗ)')

    table1Concat = {}

    if len(table1Data) > 0:
        for rzMnemonic, mnemonics in table1Data.items():
            total = 0

            for value in mnemonics.values():
                total += value

            table1Concat[rzMnemonic] = total

        # print('DEBUG', table1Concat)

    spin.start('Получение показателей дашборда «Состояние сервисов». . .')

    table2 = docTables.get('table_service_access')
    table2Data = serviceAccess(fromDate, toDate)

    spin.update('Заполнение Таблицы 2 (Значения показателей дашборда «Состояние сервисов»). . .')

    table2.Cell(2, 2).Range.Text = table2Data.get('alerts', '?')
    table2.Cell(3, 2).Range.Text = table2Data.get('availability', '?%')

    spin.ok('Таблица 2 заполнена (Значения показателей дашборда «Состояние сервисов»)')

    print('INFO Таблица 3 пропущена (Значения показателей дашборда «Доступность серверов»)')

    spin.start('Получение данных показателей дашборда «Telegraf». . .')

    table4 = docTables.get('table_telegraf')
    table4Data = telegrafMetrics(fromDate, toDate)

    spin.update('Заполнение Таблицы 4 (Значения показателей дашборда «Telegraf»). . .')

    table4.Cell(2, 2).Range.Text = table4Data.get('la_medium', '?')
    table4.Cell(3, 2).Range.Text = table4Data.get('zombies', '?')
    table4.Cell(4, 2).Range.Text = table4Data.get('processes', '?')
    table4.Cell(5, 2).Range.Text = table4Data.get('threads', '?')
    table4.Cell(6, 2).Range.Text = table4Data.get('cpuUsage', '?%')
    table4.Cell(7, 2).Range.Text = table4Data.get('ramUsage', '?%')
    table4.Cell(8, 2).Range.Text = table4Data.get('rootFsUsed', '?%')
    table4.Cell(9, 2).Range.Text = table4Data.get('ioWait', '?%')
    table4.Cell(10, 2).Range.Text = table4Data.get('tcp_connections', 'Изменение max/average ?/?')
    table4.Cell(11, 2).Range.Text = table4Data.get('network_usage', '?/?')
    table4.Cell(12, 2).Range.Text = table4Data.get('disk_usage', '? B')
    table4.Cell(13, 2).Range.Text = table4Data.get('swap_usage', '?')

    spin.ok('Таблица 4 заполнена (Значения показателей дашборда «Telegraf»)')

    print('INFO Таблица 5 пропущена (Значения показателей дашборда «System Alerts»)')

    spin.start('Получение значений показателей дашборда «PODD Query Status»')

    if len(table1Concat) > 0:
        spin.update('Заполнение Таблицы 6 (Значения показателей дашборда «PODD Query Status»). . .')

        table6 = docTables.get('table_podd_query_status')
        executeTimes = queryExecuteTime(fromDate, toDate)

        keys = table1Concat.keys()

        for rowIndex in range(2, table6.Rows.Count + 1):
            textContent = table6.Cell(rowIndex, 1).Range.Text

            for key in keys:
                if 'Количество' in textContent and key in textContent:
                    table6.Cell(rowIndex, 2).Range.Text = table1Concat.get(key, 0)
                elif 'Время' in textContent and key in textContent:
                    table6.Cell(rowIndex, 2).Range.Text = executeTimes.get(key, 0)

    spin.ok('Таблица 6 заполнена (Значения показателей дашборда «PODD Query Status»)')

    spin.start('Получение данных показателей «PODD Agent Metrics». . .')

    table7 = docTables.get('table_podd_agent_metrics')
    table7Data = alertMetrics(fromDate, toDate)

    spin.update('Заполнение Таблица 7 (Значения показателей дашборда «PODD Agent Metrics»). . .')

    table7.Cell(2, 2).Range.Text = table7Data.get('mean', '? res/s')

    spin.ok('Таблица 7 заполнена (Значения показателей дашборда «PODD Agent Metrics»)')
    spin.start('Получение данных показателей дашборда «JVM Metrics». . .')

    table8 = docTables.get('table_jvm_metrics', '')
    table8Data = jvmMetrics(fromDate, toDate)

    spin.update('Заполнение Таблица 8 (Значения показателей дашборда «JVM Metrics»). . .')

    table8.Cell(2, 2).Range.Text = table8Data.get('memory_usage', '? B')
    table8.Cell(3, 2).Range.Text = table8Data.get('collection_count', '?')
    table8.Cell(4, 2).Range.Text = table8Data.get('collection_time', '? ms')
    table8.Cell(5, 2).Range.Text = table8Data.get('memory_pool', '? B')
    table8.Cell(6, 2).Range.Text = table8Data.get('threads_count', '?')

    spin.ok('Таблица 8 заполнена (Значения показателей дашборда «JVM Metrics»)')
    spin.start('Получение показателей дашборда «ETL Delta Processed». . .')

    table91 = docTables.get('table_delta_processed')
    table91Data = kafkaLag(fromDate, toDate)

    spin.update('Заполнение Таблица 9.1 (Значения показателей дашборда «ETL Delta Processed»). . .')

    for rowIndex in range(2, table91.Rows.Count + 1):
        textContent = table91.Cell(rowIndex, 1).Range.Text

        for key, value in table91Data.items():
            if key in textContent:
                table91.Cell(rowIndex, 2).Range.Text = value

                break

    spin.ok('Таблица 9.1 заполнена (Значения показателей дашборда «ETL Delta Processed»)')

    print('INFO Таблица 9.2 пропущена (Показатель «Разница в количестве строк от прошлого замера в таблицах»)')

    spin.start('Получение показателей зарегистрированных событий. . .')

    table10 = docTables.get('table_events')
    table10Data = eventsStatus(fromDate, toDate)

    spin.update('Заполнение Таблицы 10 (Зарегистрированные события). . .')

    table10.Cell(2, 2).Range.Text = table10Data.get('debug', '?')
    table10.Cell(3, 2).Range.Text = table10Data.get('info', '?')
    table10.Cell(4, 2).Range.Text = table10Data.get('warn', '?')

    spin.ok('Таблица 10 заполнена (Зарегистрированные события)')

    if isExe():
        spin.start(f'Выгрузка изображений дашбордов. . .')

        images = dashboardImages(fromDate, toDate)

        spin.update('Вставка изображений дашбордов. . .')

        for imageKey, imagePath in images.items():
            bookmarkImage = document.Bookmarks[imageKey].Range

            if bookmarkImage is None:
                continue

            bookmarkImage.Text = ''
            document.Bookmarks.Add(imageKey, bookmarkImage)
            bookmarkImage.InlineShapes.AddPicture(imagePath, False, True)

        shutil.rmtree(storagePath('./_images'))

        spin.ok(f'Вставка изображений завершена {len(images)}/{webPagesLen()}')
    else:
        print('INFO Выгрузка изображений дашбордов пропущена')

    return document

def main(fromDate, toDate, isDaily) -> str:
    if type(fromDate) != arrow.arrow.Arrow:
        raise Exception('Invalid parameter: fromDate.')

    if type(toDate) != arrow.arrow.Arrow:
        raise Exception('Invalid parameter: toDate.')

    templatePath = storagePath('./templates/monitoring_template.docx')

    if not path.exists(templatePath):
        print(f'FAIL Шаблон документа метрики РЗ не обнаружен в {templatePath}')

        return False

    reportDate = fromDate.format("DD.MM.YYYY")

    if not isDaily:
        reportDate = monthName(fromDate) + ' ' + fromDate.format("YYYY")

    print(f'INFO Создание отчета мониторинга РЗ за {reportDate}')

    spin.start('Подготовка к созданию отчета. . .')

    try:
        msWord = client.DispatchEx('Word.Application')
        msWord.Visible = 0

        msDocx = msWord.Documents.Open(templatePath)

        try:
            msDocx.CustomDocumentProperties('ReportDate').value = reportDate
            msDocx.CustomDocumentProperties('CurrentDate').value = arrow.now().format('DD.MM.YYYY')
            msDocx.Fields.Update()

            for l in range(0, msDocx.TablesOfContents.Count):
                msDocx.TablesOfContents(l + 1).Update()

            document = __fillInTemplate(msDocx, fromDate, toDate)

            if document is None:
                return False

            spin.start('Сохранение отчета. . .')

            fileName = f'{env.MONIT_FILE_NAME} за {reportDate}.docx'
            saveAs = path.join(env.OUT_DIR, fileName)

            document.SaveAs(saveAs)

            spin.ok(f'Отчет сохранен как: {saveAs}')
        except Exception as error:
            print(f'MsW Error: {error}')

            stacktrace()

            return False
    except Exception as error:
        print(f'App Error: {error}')

        stacktrace()

        return False
    finally:
        spin.stop()

        msWord.ActiveDocument.Close(False)
        msWord.Quit(False)

    return True
