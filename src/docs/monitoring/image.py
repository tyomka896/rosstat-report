import arrow
import asyncio
from os import path, makedirs
from pyppeteer import launch
from pyppeteer.network_manager import Request

from utils.spin import spin
from utils.grafana.main import Grafana
from config.env import grafanaUrl, storagePath

def win_chrome_path():
    for l in range(2):
        chrome_file = 'C:\\Program Files' + (' (x86)' if l else '') +'\\Google\\Chrome\\Application\\chrome.exe'

        if path.isfile(chrome_file):
            return path.normpath(chrome_file)

    return None

def __webPagesList(fromDate, toDate) -> list:
    return [
        {
            'url': grafanaUrl(f'/d/ZmCMLrcVk/sostoianie-servisov-komponentov-vitriny-dannykh-nsud?from={fromDate}&to={toDate}'),
            'viewport': { 'width': 1920, 'height': 1210 },
            'image': '1 global-status.png',
            'bookmark': 'image_global_status',
        },
        {
            'url': grafanaUrl(f'/d/J6zbkHu7z/dostupnost-serverov-vitriny-dannykh-nsud?from={fromDate}&to={toDate}'),
            'viewport': { 'width': 1920, 'height': 900 },
            'image': '2 service-access.png',
            'bookmark': 'image_service_access',
        },
        {
            'url': grafanaUrl(f'/d/000000128/telegraf-system-dashboard-metriki-velichiny-utiliziruemykh-sistemnykh-resursov?from={fromDate}&to={toDate}'),
            'viewport': { 'width': 1920, 'height': 635 },
            'image': '3 telegraf.png',
            'bookmark': 'image_telegraf',
        },
        {
            'url': grafanaUrl(f'/d/jN17p_-Vz/system-alerts-metriki-porogovykh-znachenii-vazhnykh-ekspluatatsionnykh-pokazatelei-sistemnykh-resursov?from={fromDate}&to={toDate}'),
            'viewport': { 'width': 1920, 'height': 980 },
            'image': '4 system-alert.png',
            'bookmark': 'image_system_alert',
        },
        {
            'url': grafanaUrl(f'/d/fcXGw-mSz/podd-query-status-status-vypolneniia-i-rezhima-raboty-reglamentirovannykh-zaprosov-k-vitrine-nsud-potrebiteliami-dannykh?from={fromDate}&to={toDate}'),
            'viewport': { 'width': 1920, 'height': 1330 },
            'image': '5 podd-query-status.png',
            'bookmark': 'image_podd_query_status',
        },
        {
            'url': grafanaUrl(f'/d/-_kQXZ54k/bba68dd6-5946-5dc5-8a6a-786e9e8a845e?from={fromDate}&to={toDate}'),
            'viewport': { 'width': 1920, 'height': 750 },
            'image': '6 podd-agent-metrics.png',
            'bookmark': 'image_podd_agent_metrics',
        },
        {
            'url': grafanaUrl(f'/d/3JFJSzy4k/jvm-metrics-pokazateli-funktsionirovaniia-sluzhb-vitriny-nsud-pamiat-sbor-musora-potoki?from={fromDate}&to={toDate}'),
            'viewport': { 'width': 1920, 'height': 710 },
            'image': '7 jvm-metrics.png',
            'bookmark': 'image_jvm_metrics',
        },
        {
            'url': grafanaUrl(f'/d/sKKOUscVk/etl-delta-processed-ctatus-zagruzki-dannykh-iz-podsistemy-analiticheskaia-vitrina-gis-tsap?from={fromDate}&to={toDate}'),
            'viewport': { 'width': 1920, 'height': 855 },
            'image': '8 elt-delta.png',
            'bookmark': 'image_elt_delta',
        }
    ]

async def __onRequest(request: Request):
    request.headers.update(Grafana.getAuthHeader())

    await request.continue_(overrides={ 'headers': request.headers })

async def __poddQueryStatusRzImageAsync(webPages):
    images = {}

    if not path.exists(storagePath(f'_images')):
        makedirs(storagePath(f'_images'))

    browser = await launch({
        # 'headless': False,
        # 'devtools': True,
        'args': [ '--proxy-server=http=socks4://172.24.25.222:1080' ],
        'executablePath': win_chrome_path(),
    })

    try:
        for l, webPage in enumerate(webPages):
            spin.update(f'Выгрузка изображений дашбордов {l + 1}/{len(webPages)}. . .')

            page = await browser.newPage()

            page.setDefaultNavigationTimeout(120000)

            await page.setViewport(webPage['viewport'])

            await page.setRequestInterception(True)
            page.on('request', lambda request: asyncio.create_task(__onRequest(request)))

            imageName = webPage['image']

            saveAs = storagePath(f'_images/{webPage["image"]}')

            try:
                await page.goto(webPage['url'], { 'waitUntil': 'networkidle0' })

                images[webPage['bookmark']] = saveAs

                await page.screenshot({ 'path': saveAs })
            except TimeoutError as _:
                images[webPage['bookmark']] = saveAs

                await page.screenshot({ 'path': saveAs })

                print(f' (timeout: {imageName})')
            except:
                imageName = webPage['image']

                print(f' Не удалось выгрузить \'{imageName}\'')
            finally:
                await page.close()
    finally:
        await browser.close()

    return images

def webPagesLen() -> int:
    return __webPagesList('', '').__len__()

def dashboardImages(fromDate, toDate) -> str:
    if type(fromDate) != arrow.arrow.Arrow:
        raise Exception('Invalid parameter: fromDate.')

    if type(toDate) != arrow.arrow.Arrow:
        raise Exception('Invalid parameter: toDate.')

    webPages = __webPagesList(
        fromDate.format('x')[0:-3],
        toDate.format('x')[0:-3],
    )

    return (
        asyncio
            .get_event_loop()
            .run_until_complete(
                __poddQueryStatusRzImageAsync(webPages)
            )
    )
