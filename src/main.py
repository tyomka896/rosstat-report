import re
import arrow
import inquirer
import traceback

from config.env import env
from utils.spin import spin
from utils.helpers import exitApp
from utils.grafana.main import Grafana
from utils.graylog.main import Graylog
import docs.loading.main as loading
import docs.monitoring.main as monitoring

def checkApiConnection() -> bool:
    spin.start('Проверка соединения с Grafana. . . ')

    if Grafana.checkConnection():
        spin.ok('Соединение с Grafana установлено.')
    else:
        spin.fail(f'Не удалось установить соединение с Grafana ({env.GRAFANA_URL})')

        return False

    spin.start('Проверка соединения с Graylog. . . ')

    if Graylog.checkConnection():
        spin.ok('Соединение с Graylog установлено.')
    else:
        spin.fail(f'Не удалось установить соединение с Graylog ({env.GRAYLOG_URL})')

        return False

    return True

def askDateReport() -> dict:
    current = arrow.now().to('Europe/Moscow').shift(days=-1).floor('day')

    _result = {
        'fromDate': current,
        'toDate': current.ceil('day'),
        'daily': True,
    }

    dateInput = None

    try:
        dateInput = inquirer.prompt([
            inquirer.Text(
                'value',
                message=f'Введите дату составления отчета ({current.format("DD.MM.YYYY")})',
            )
        ], raise_keyboard_interrupt=True)
    except KeyboardInterrupt:
        return { 'cancel': True }

    dateValue = dateInput['value'].strip()

    if re.match('^\d{1,2}\.\d{1,2}$', dateValue):
        day, month = dateValue.split('.')

        _result['fromDate'] = current.replace(day=int(day), month=int(month)).floor('day')
        _result['toDate'] = current.replace(day=int(day), month=int(month)).ceil('day')
    elif re.match('^\d{1,2}\.\d{4}$', dateValue):
        month, year = dateValue.split('.')

        _result['fromDate'] = current.replace(month=int(month), year=int(year)).floor('month')
        _result['toDate'] = current.replace(month=int(month), year=int(year)).ceil('month')
        _result['daily'] = False
    elif re.match('^\d{1,2}\.\d{1,2}\.\d{4}$', dateValue):
        day, month, year = dateValue.split('.')

        _result['fromDate'] = current.replace(day=int(day), month=int(month), year=int(year)).floor('day')
        _result['toDate'] = current.replace(day=int(day), month=int(month), year=int(year)).ceil('day')
    elif dateValue.__len__() > 0:
        return None

    return _result

def selectReportType() -> str:
    choices = {
        'Все отчеты': 'all',
        'Мониторинг РЗ': 'monitoring',
        'Регламентная загрузка': 'loading',
        'Выход': 'exit',
    }

    print()

    reportInput = inquirer.prompt([
        inquirer.List(
            'value',
            message='Выберите вариант отчета',
            choices=choices.keys(),
        )
    ])

    reportValue = reportInput['value']

    return choices.get(reportValue) or ''

def main():
    print('СОЗДАНИЕ ОТЧЕТОВ В РАМКАХ РОССТАТА (@tyomka896)')
    print('----------------------------------------------------------------')

    while True:
        if checkApiConnection():
            break

        select = inquirer.prompt([
            inquirer.Confirm(
                'value',
                message='Повторить установку соединения?',
                default=True
            )
        ])

        if select is None or not select.get('value'):
            exitApp(1)

    while True:
        reportType = selectReportType()

        if reportType == 'exit':
            exitApp(0)

        dateInfo = None

        try:
            dateInfo = askDateReport()
        except Exception as error:
            print('Не верная дата:', str(error))

            continue

        if dateInfo is None:
            dayExample = arrow.now().shift(days=-1).format("DD.MM.YYYY")
            monthExample = arrow.now().shift(months=-1).format("MM.YYYY")

            print('Не верный формат даты. Допускается:')
            print(f' - {dayExample} - создать отчет за один день')
            print(f' -    {monthExample} - создать отчет за весь месяц')

            continue
        elif dateInfo.get('cancel'):
            continue

        fromDate = dateInfo['fromDate']
        toDate = dateInfo['toDate']
        isDaily = dateInfo['daily']

        match reportType:
            case 'monitoring': monitoring.main(fromDate, toDate, isDaily)
            case 'loading': loading.main(fromDate, toDate, isDaily)
            case 'all':
                monitoring.main(fromDate, toDate, isDaily)

                print()

                loading.main(fromDate, toDate, isDaily)
            case _: continue

def main_dbg():
    date = arrow.get(1698395681)
    print('date', date)

if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt as error:
        exitApp(1)
    except Exception as error:
        print('Error:', str(error))

        exitApp(2)

    exitApp(0)
