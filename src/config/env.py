from os import path, curdir, makedirs
from urllib.parse import urljoin
from dotenv import dotenv_values

from utils.helpers import isExe

class __env():
    __grafanaUrl = 'http://172.16.174.188:3000'
    __grafanaUsername = ''
    __grafanaPassword = ''
    __graylogUrl = 'http://172.16.174.188:9000'
    __graylogUsername = ''
    __graylogPassword = ''
    __storagePath = ''
    __outDir = ''
    __monitFileName = 'Росстат. Справка-отчёт мониторинга РЗ'
    __loadFileName = 'Росстат. Справка о регламентной загрузке'

    def __init__(self) -> None:
        env_values = dotenv_values('.env' if isExe() else '.env.dev')

        if env_values.get('GRAFANA_URL'):
            self.__grafanaUrl = env_values.get('GRAFANA_URL')

        if env_values.get('GRAFANA_USERNAME'):
            self.__grafanaUsername = env_values.get('GRAFANA_USERNAME')

        if env_values.get('GRAFANA_PASSWORD'):
            self.__grafanaPassword = env_values.get('GRAFANA_PASSWORD')

        if env_values.get('GRAYLOG_URL'):
            self.__graylogUrl = env_values.get('GRAYLOG_URL')

        if env_values.get('GRAYLOG_USERNAME'):
            self.__graylogUsername = env_values.get('GRAYLOG_USERNAME')

        if env_values.get('GRAYLOG_PASSWORD'):
            self.__graylogPassword = env_values.get('GRAYLOG_PASSWORD')

        if env_values.get('MONIT_FILE_NAME'):
            self.__monitFileName = env_values.get('MONIT_FILE_NAME').strip()

        if env_values.get('LOAD_FILE_NAME'):
            self.__loadFileName = env_values.get('LOAD_FILE_NAME').strip()

        self.setStoragePath()
        self.setOutDir(env_values.get('OUT_DIR'))

    @property
    def STORAGE_PATH(self):
        return self.__storagePath

    @property
    def OUT_DIR(self):
        return self.__outDir

    @property
    def GRAFANA_URL(self) -> str:
        return self.__grafanaUrl

    @property
    def GRAFANA_USERNAME(self) -> str:
        return self.__grafanaUsername

    @property
    def GRAFANA_PASSWORD(self) -> str:
        return self.__grafanaPassword

    @property
    def GRAYLOG_URL(self) -> str:
        return self.__graylogUrl

    @property
    def GRAYLOG_USERNAME(self) -> str:
        return self.__graylogUsername

    @property
    def GRAYLOG_PASSWORD(self) -> str:
        return self.__graylogPassword

    @property
    def MONIT_FILE_NAME(self) -> str:
        return self.__monitFileName

    @property
    def LOAD_FILE_NAME(self) -> str:
        return self.__loadFileName

    def setStoragePath(self):
        result = 'storage' if isExe() else 'src\\storage'

        result = path.join(path.abspath(curdir), result)

        if not path.exists(result):
            makedirs(result)

        self.__storagePath = result

    def setOutDir(self, value):
        if value is not None and path.exists(value):
            self.__outDir = path.abspath(value)
        elif isExe():
            self.__outDir = path.abspath(curdir)

            print(f'DEBUG Директория сохранения текущая.')
        else:
            self.__outDir = self.__storagePath

            print(f'DEBUG Директория сохранения: {self.__outDir}')

def storagePath(fileName = None) -> str:
    result = env.STORAGE_PATH

    if type(fileName) == str and fileName != '':
        result = path.join(result, fileName)

    return path.normpath(result)

def grafanaUrl(url = None) -> str:
    if url and type(url) == str:
        return urljoin(env.GRAFANA_URL, url)

    return env.GRAFANA_URL

def graylogUrl(url = None) -> str:
    if url and type(url) == str:
        return urljoin(env.GRAYLOG_URL, url)

    return env.GRAYLOG_URL

env = __env()
