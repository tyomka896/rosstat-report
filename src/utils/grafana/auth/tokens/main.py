import json
from os import path
from pathlib import Path as pathlib

from config.env import storagePath
from utils.grafana.auth.tokens.token import Token

TOKEN_EXPIRE_IN_SEC = 600

class Tokens():
    def __init__(self):
        self._accessToken = None
        self._tokenPath = path.join(storagePath(), 'grafana_token.json')

        if path.isfile(self._tokenPath):
            try:
                tokenContent = json.loads(pathlib(self._tokenPath).read_text())

                grafana_token = tokenContent.get('access_token')

                if grafana_token is not None:
                    self._accessToken = Token(
                        grafana_token.get('value'),
                        grafana_token.get('expires_at'),
                    )
            except:
                pass

    @property
    def tokenPath(self):
        return self._tokenPath

    @property
    def accessToken(self):
        return getattr(self._accessToken, 'value', None)

    def tokens(self, obj):
        if obj is None:
            raise Exception('Invalid parameter: obj.')

        access_token = obj.get('access_token')

        if access_token is None:
            raise Exception('Invalid parameter: obj.')

        expires_at = obj.get('expires_at')

        self._accessToken = Token(
            access_token,
            expires_at or TOKEN_EXPIRE_IN_SEC,
        )

        with open(self._tokenPath, 'w') as file:
            jsonToWrite = json.dumps({
                'access_token': {
                    'value': self._accessToken.value,
                    'expires_at': self._accessToken.expiresAt.isoformat(),
                },
            }, indent=2)

            file.write(jsonToWrite)
