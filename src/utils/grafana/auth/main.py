import json
import arrow

from config.env import env, grafanaUrl
from utils.request import request, requestExceptions
from utils.helpers import exitApp
from utils.grafana.auth.tokens.main import Tokens

class Auth(Tokens):
    def __init__(self):
        super().__init__()

        self._headers = {
            'Content-Type': 'application/json'
        }

    def getAuthHeader(self):
        _token = self.getAccessToken()

        if _token:
            return {
                'Cookie': f'grafana_session={_token}',
                'Content-Type': 'application/json',
            }
        else:
            return None

    def getAccessToken(self):
        _token = self.accessToken

        if _token is not None:
            return _token

        self._requestAccessToken()

        return self.accessToken

    def _requestAccessToken(self):
        try:
            response = request.post(
                grafanaUrl('/login'),
                data=json.dumps({
                    'user': env.GRAFANA_USERNAME,
                    'password': env.GRAFANA_PASSWORD,
                }),
                headers=self._headers,
            )
            response.raise_for_status()

            access_token = response.cookies.get_dict().get('grafana_session')

            self.tokens({
                'access_token': access_token,
            })
        except requestExceptions.HTTPError as error:
            print(f'FAIL Не удалось авторизоваться в Grafana с ошибкой: {error.response.json()}')

            exitApp(2)
        except requestExceptions.RequestException as error:
            print(f'Error to retrieve access token: {str(error)}')

            exitApp(2)
