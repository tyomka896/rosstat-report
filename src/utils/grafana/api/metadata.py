import json
import arrow

from config.env import grafanaUrl
from utils.grafana.api.main import handleResponse
from utils.request import request, requestExceptions

class Metadata():
    def __makeDsRequest(self, fromDate, toDate, queries, methodName='ds-query'):
        if type(fromDate) != arrow.arrow.Arrow:
            raise Exception('Invalid parameter: fromDate.')

        if type(toDate) != arrow.arrow.Arrow:
            raise Exception('Invalid parameter: toDate.')

        _fromDate = fromDate.format('x')[0:-3]
        _toDate = toDate.format('x')[0:-3]

        try:
            response = request.post(
                grafanaUrl('/api/ds/query'),
                data=json.dumps({
                    'queries': queries,
                    'from': _fromDate,
                    'to': _toDate,
                }),
                headers=self.getAuthHeader(),
                timeout=30,
            )
            response.raise_for_status()

            return handleResponse(response)
        except requestExceptions.HTTPError as error:
            return handleResponse(response)
        except Exception as error:
            print(f'Error to get {methodName}: {str(error)}')

        return None

    def __makeDatasourceRequest(self, fromDate, toDate, query, methodName='datasource'):
        if type(fromDate) != arrow.arrow.Arrow:
            raise Exception('Invalid parameter: fromDate.')

        if type(toDate) != arrow.arrow.Arrow:
            raise Exception('Invalid parameter: toDate.')

        if type(query) != str:
            raise Exception('Invalid parameter: query.')

        _fromDate = fromDate.format('x')[0:-3]
        _toDate = toDate.format('x')[0:-3]

        _query = query.replace('::from_date', _fromDate).replace('::to_date', _toDate)

        try:
            response = request.get(
                grafanaUrl('/api/datasources/proxy/1/query'),
                params={
                    'q': _query,
                    'db': 'telegraf',
                    'epoch': 'ms',
                },
                headers=self.getAuthHeader(),
                timeout=30,
            )
            response.raise_for_status()

            return handleResponse(response)
        except requestExceptions.HTTPError as error:
            return handleResponse(response)
        except Exception as error:
            print(f'Error to get {methodName}: {str(error)}')

        return None

    def queryStatus(self, fromDate, toDate):
        queries = [{
            'refId': 'B',
            'datasourceId': 2,
            'rawSql': "SELECT\n  $__timeGroupAlias(execution_start::timestamp AT TIME ZONE 'UTC-3',1d, 0),\n  source,\n  reg_query,\n  count(message_id) AS \"Успешных РЗ за период\"\nFROM logparser.p_message\nWHERE\n  $__timeFilter(execution_start::timestamp AT TIME ZONE 'UTC-3') AND execution_end notnull AND execution_length notnull AND sending_length notnull AND execution_error isnull AND source <> 'GKSR03'\nGROUP BY time, source, reg_query\nORDER BY 1",
            'format': 'table',
            'intervalMs': 300000,
            'maxDataPoints': 1956
        }]

        return self.__makeDsRequest(
            fromDate=fromDate,
            toDate=toDate,
            queries=queries,
            methodName='queryStatus',
        )

    def queryExecuteTime(self, fromDate, toDate):
        queries = [{
            'refId': 'A',
            'datasourceId': 2,
            'rawSql': "SELECT $__time(p_mes.execution_start::timestamp AT TIME ZONE 'UTC-3'), p_mes.reg_query, p_mes.execution_length AS execution_length FROM logparser.p_message p_mes WHERE $__timeFilter(p_mes.execution_start::timestamp AT TIME ZONE 'UTC-3') AND p_mes.execution_end notnull AND p_mes.execution_length notnull AND p_mes.sending_length notnull AND p_mes.execution_error isnull AND p_mes.source <> 'GKSR03' ORDER BY 1",
            'format': 'time_series',
            'intervalMs': 60000,
            'maxDataPoints': 1956,
        }]

        return self.__makeDsRequest(
            fromDate=fromDate,
            toDate=toDate,
            queries=queries,
            methodName='queryExecuteTime',
        )

    def alertMetrics(self, fromDate, toDate):
        queries = [{
            "datasource": {
                "type": "prometheus",
                "uid": "LdbSE0s4z"
            },
            "editorMode": "code",
            "expr": "rate(messages_core2agent_count_total{}[5m]) ",
            "instant": False,
            "legendFormat": "__auto",
            "range": False,
            "refId": "A",
            "exemplar": False,
            "requestId": "45A",
            "utcOffsetSec": 25200,
            "interval": "",
            "datasourceId": 4,
            "intervalMs": 30000,
            "maxDataPoints": 2016
        }]

        return self.__makeDsRequest(
            fromDate=fromDate,
            toDate=toDate,
            queries=queries,
            methodName='alertMetrics',
        )

    def serviceCurrentAlert(self, fromDate, toDate):
        return self.__makeDatasourceRequest(
            fromDate=fromDate,
            toDate=toDate,
            query='SELECT count("active_code") FROM "systemd_units" WHERE ("host" =~ /^(rst-dap-nsud-adapter-1\.dap\.local|rst-dap-nsud-adapter-2\.dap\.local|rst-dap-nsud-etl-1\.dap\.local|rst-dap-nsud-etl-2\.dap\.local|rst-dap-nsud-kafka-1\.dap\.local|rst-dap-nsud-kafka-2\.dap\.local|rst-dap-nsud-kafka-3\.dap\.local|rst-dap-nsud-mon-1\.dap\.local|rst-dap-nsud-podd-1\.dap\.local|rst-dap-nsud-prostore-1\.dap\.local)$/ AND "name" =~ /^(elasticsearch\.service|etl-rosstat001\.service|filebeat\.service|graylog-server\.service|kafka-postgres-reader\.service|kafka-postgres-writer\.service|kafka\.service|podd-adapter-group-repl\.service|podd-adapter-group-tp\.service|podd-adapter-import-tp\.service|podd-adapter-mppr\.service|podd-adapter-mppw\.service|podd-adapter-query\.service|podd-adapter-replicator\.service|query-execution\.service|s3-uploader\.service|status-monitor\.service|zookeeper\.service)$/) AND "active_code" > 0 AND time >= ::from_datems and time <= ::to_datems GROUP BY time(5m) fill(null)',
            methodName='serviceCurrentAlert',
        )

    def serviceGlobalAvailability(self, fromDate, toDate):
        return self.__makeDatasourceRequest(
            fromDate=fromDate,
            toDate=toDate,
            query='SELECT 100-(sum("active_code") * 100 / count("active_code")) FROM (SELECT ("active_code" / "active_code") AS "active_code" FROM "systemd_units" WHERE "host"=~/^(rst-dap-nsud-adapter-1\.dap\.local|rst-dap-nsud-adapter-2\.dap\.local|rst-dap-nsud-etl-1\.dap\.local|rst-dap-nsud-etl-2\.dap\.local|rst-dap-nsud-kafka-1\.dap\.local|rst-dap-nsud-kafka-2\.dap\.local|rst-dap-nsud-kafka-3\.dap\.local|rst-dap-nsud-mon-1\.dap\.local|rst-dap-nsud-podd-1\.dap\.local|rst-dap-nsud-prostore-1\.dap\.local)$/ AND "name" =~ /^(elasticsearch\.service|etl-rosstat001\.service|filebeat\.service|graylog-server\.service|kafka-postgres-reader\.service|kafka-postgres-writer\.service|kafka\.service|podd-adapter-group-repl\.service|podd-adapter-group-tp\.service|podd-adapter-import-tp\.service|podd-adapter-mppr\.service|podd-adapter-mppw\.service|podd-adapter-query\.service|podd-adapter-replicator\.service|query-execution\.service|s3-uploader\.service|status-monitor\.service|zookeeper\.service)$/ AND time >= ::from_datems and time <= ::to_datems)',
            methodName='serviceGlobalAvailability',
        )

    def telegrafLaMedium(self, fromDate, toDate):
        return self.__makeDatasourceRequest(
            fromDate=fromDate,
            toDate=toDate,
            query='SELECT last("load5") FROM "system" WHERE ("host" =~ /^rst-dap-nsud-adapter-1\.dap\.local$/) AND time >= ::from_datems and time <= ::to_datems GROUP BY time(5m) fill(null)',
            methodName='telegrafLaMedium',
        )

    def telegrafZombies(self, fromDate, toDate):
        return self.__makeDatasourceRequest(
            fromDate=fromDate,
            toDate=toDate,
            query='SELECT last("zombies") FROM "processes" WHERE ("host" =~ /^rst-dap-nsud-adapter-1\.dap\.local$/) AND time >= ::from_datems and time <= ::to_datems GROUP BY time(5m) fill(null)',
            methodName='telegrafZombies',
        )

    def telegrafProcesses(self, fromDate, toDate):
        return self.__makeDatasourceRequest(
            fromDate=fromDate,
            toDate=toDate,
            query='SELECT last("total") FROM "processes" WHERE ("host" =~ /^rst-dap-nsud-adapter-1\.dap\.local$/) AND time >= ::from_datems and time <= ::to_datems GROUP BY time(5m) fill(null)',
            methodName='telegrafProcesses',
        )

    def telegrafThreads(self, fromDate, toDate):
        return self.__makeDatasourceRequest(
            fromDate=fromDate,
            toDate=toDate,
            query='SELECT last("total_threads") FROM "processes" WHERE ("host" =~ /^rst-dap-nsud-adapter-1\.dap\.local$/) AND time >= ::from_datems and time <= ::to_datems GROUP BY time(5m) fill(null)',
            methodName='telegrafThreads',
        )

    def telegrafCpuUsage(self, fromDate, toDate):
        return self.__makeDatasourceRequest(
            fromDate=fromDate,
            toDate=toDate,
            query='SELECT last("usage_idle") * -1 + 100 FROM "cpu" WHERE ("host" =~ /^rst-dap-nsud-adapter-1\.dap\.local$/ AND "cpu" = \'cpu-total\') AND time >= ::from_datems and time <= ::to_datems GROUP BY time(5m) fill(null)',
            methodName='telegrafCpuUsage',
        )

    def telegrafRamUsage(self, fromDate, toDate):
        return self.__makeDatasourceRequest(
            fromDate=fromDate,
            toDate=toDate,
            query='SELECT last("used_percent") FROM "mem" WHERE ("host" =~ /^rst-dap-nsud-adapter-1\.dap\.local$/) AND time >= ::from_datems and time <= ::to_datems GROUP BY time(5m) fill(null)',
            methodName='telegrafRamUsage',
        )

    def telegrafRootFsUsed(self, fromDate, toDate):
        return self.__makeDatasourceRequest(
            fromDate=fromDate,
            toDate=toDate,
            query='SELECT last("used_percent") FROM "disk" WHERE ("host" =~ /^rst-dap-nsud-adapter-1\.dap\.local$/ AND "path" = \'/\') AND time >= ::from_datems and time <= ::to_datems GROUP BY time(5m) fill(null)',
            methodName='telegrafRootFsUsed',
        )

    def telegrafIoWait(self, fromDate, toDate):
        return self.__makeDatasourceRequest(
            fromDate=fromDate,
            toDate=toDate,
            query='SELECT last("usage_iowait") FROM "cpu" WHERE ("host" =~ /^rst-dap-nsud-adapter-1\.dap\.local$/ AND "cpu" = \'cpu-total\') AND time >= ::from_datems and time <= ::to_datems GROUP BY time(5m) fill(null)',
            methodName='telegrafIoWait',
        )

    def telegrafTcpConnections(self, fromDate, toDate):
        return self.__makeDatasourceRequest(
            fromDate=fromDate,
            toDate=toDate,
            query='SELECT mean(tcp_close) as CLOSED, mean(tcp_close_wait) as CLOSE_WAIT, mean(tcp_closing) as CLOSING, mean(tcp_established) as ESTABLISHED, mean(tcp_fin_wait1) as FIN_WAIT1, mean(tcp_fin_wait2) as FIN_WAIT2, mean(tcp_last_ack) as LAST_ACK, mean(tcp_syn_recv) as SYN_RECV, mean(tcp_syn_sent) as SYN_SENT, mean(tcp_time_wait) as TIME_WAIT FROM "netstat" WHERE host =~ /rst-dap-nsud-adapter-1.dap.local$/ AND time >= ::from_datems and time <= ::to_datems GROUP BY time(5m), host ORDER BY asc',
            methodName='telegrafTcpConnections',
        )

    def telegrafNetworkUsage(self, fromDate, toDate):
        return self.__makeDatasourceRequest(
            fromDate=fromDate,
            toDate=toDate,
            query='SELECT non_negative_derivative(mean(bytes_recv),1s)*8 as "in"  FROM "net" WHERE host =~ /rst-dap-nsud-adapter-1.dap.local/ AND interface =~ /eth0/ AND time >= ::from_datems and time <= ::to_datems GROUP BY time(1m), * fill(none);SELECT non_negative_derivative(mean(bytes_sent),1s)*8 as "out" FROM "net" WHERE host =~ /rst-dap-nsud-adapter-1.dap.local/ AND interface =~ /eth0/ AND time >= ::from_datems and time <= ::to_datems GROUP BY time(5m), * fill(none)',
            methodName='telegrafNetworkUsage',
        )

    def telegrafDiskUsage(self, fromDate, toDate):
        return self.__makeDatasourceRequest(
            fromDate=fromDate,
            toDate=toDate,
            query='SELECT mean(total) AS "total", mean(used) as "used" FROM "disk" WHERE "host" =~ /rst-dap-nsud-adapter-1.dap.local$/ AND "path" =~ /^\/$/AND time >= ::from_datems and time <= ::to_datems GROUP BY time(5m), "host", "path"',
            methodName='telegrafDiskUsage',
        )

    def telegrafSwapUsage(self, fromDate, toDate):
        return self.__makeDatasourceRequest(
            fromDate=fromDate,
            toDate=toDate,
            query='SELECT last("used_percent") FROM "swap" WHERE ("host" =~ /^rst-dap-nsud-adapter-1\.dap\.local$/) AND time >= ::from_datems and time <= ::to_datems GROUP BY time(5m) fill(null)',
            methodName='telegrafSwapUsage',
        )

    def jvmMemoryUsage(self, fromDate, toDate):
        return self.__makeDatasourceRequest(
            fromDate=fromDate,
            toDate=toDate,
            query='SELECT mean("HeapMemoryUsage.used") FROM "java_memory" WHERE ("host" =~ /^rst-dap-nsud-etl-1.dap.local$/ AND "application" =~ /^etl-rosstat001$/) AND time >= ::from_datems and time <= ::to_datems GROUP BY time(5m) fill(none)',
            methodName='jvmMemoryUsage',
        )

    def jvmGcCollectionCount(self, fromDate, toDate):
        return self.__makeDatasourceRequest(
            fromDate=fromDate,
            toDate=toDate,
            query='SELECT difference("CollectionCount") FROM "java_garbage_collector" WHERE ("host" =~ /^rst-dap-nsud-etl-1.dap.local$/ AND "application" =~ /^etl-rosstat001$/) AND time >= ::from_datems and time <= ::to_datems GROUP BY "name"',
            methodName='jvmGcCollectionCount',
        )

    def jvmGcCollectionTime(self, fromDate, toDate):
        return self.__makeDatasourceRequest(
            fromDate=fromDate,
            toDate=toDate,
            query='SELECT difference("CollectionTime") FROM "java_garbage_collector" WHERE ("host" =~ /^rst-dap-nsud-etl-1.dap.local$/ AND "application" =~ /^etl-rosstat001$/) AND time >= ::from_datems and time <= ::to_datems GROUP BY "name"',
            methodName='jvmGcCollectionTime',
        )

    def jvmMemoryPool(self, fromDate, toDate):
        return self.__makeDatasourceRequest(
            fromDate=fromDate,
            toDate=toDate,
            query='SELECT mean("CollectionUsage.committed") FROM "java_memory_pool" WHERE ("host" =~ /^rst-dap-nsud-etl-1.dap.local$/ AND "application" =~ /^etl-rosstat001$/) AND time >= ::from_datems and time <= ::to_datems GROUP BY time(5m) fill(none)',
            methodName='jvmMemoryPool',
        )

    def jvmThreadsCount(self, fromDate, toDate):
        return self.__makeDatasourceRequest(
            fromDate=fromDate,
            toDate=toDate,
            query='SELECT mean("ThreadCount") FROM "java_threading" WHERE ("host" =~ /^rst-dap-nsud-etl-1.dap.local$/ AND "application" =~ /^etl-rosstat001$/) AND time >= ::from_datems and time <= ::to_datems GROUP BY time(5m) fill(none)',
            methodName='jvmThreadsCount',
        )

    def eltDeltaStatus(self, fromDate, toDate):
        return self.__makeDatasourceRequest(
            fromDate=fromDate,
            toDate=toDate,
            query='SELECT max("count") FROM "prostore_load_data" WHERE time >= ::from_datems and time <= ::to_datems GROUP BY time(5m), "query" fill(none)',
            methodName='eltDeltaStatus',
        )

    def eltDeltaKafkaLag(self, fromDate, toDate):
        return self.__makeDatasourceRequest(
            fromDate=fromDate,
            toDate=toDate,
            query='SELECT sum("lag") FROM "kafka_lag" WHERE time >= ::from_datems and time <= ::to_datems GROUP BY time(5m), "host", "topic" fill(null)',
            methodName='eltDeltaKafkaLag',
        )
