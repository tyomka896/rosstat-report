import json
import uuid
import arrow
import random

from config.env import graylogUrl
from utils.grafana.api.main import handleResponse
from utils.request import request, requestExceptions

class Metadata():
    def __init__(self) -> None:
        self._hexID = None

    @property
    def HEX_ID(self):
        if self._hexID is None:
            self._hexID = ''.join(random.choice('0123456789abcdef') for _ in range(24))

        return self._hexID

    def __uuid4(self):
        return str(uuid.uuid4())

    def viewsSearchExecute(self, queryString, fromDate, toDate):
        if type(queryString) != str:
            raise Exception('Invalid parameter: queryString.')

        if type(fromDate) != arrow.arrow.Arrow:
            raise Exception('Invalid parameter: fromDate.')

        if type(toDate) != arrow.arrow.Arrow:
            raise Exception('Invalid parameter: toDate.')

        try:
            searchResponse = request.post(
                graylogUrl('/api/views/search'),
                data=json.dumps({
                    "id": self.HEX_ID,
                    "queries": [
                        {
                            "id": self.__uuid4(),
                            "query": {
                                "type": "elasticsearch",
                                "query_string": queryString,
                            },
                            "timerange": {
                                "type": "absolute",
                                "from": str(fromDate),
                                "to": str(toDate),
                            },
                            "search_types": [
                                {
                                    "timerange": None,
                                    "query": None,
                                    "streams": [],
                                    "id": self.__uuid4(),
                                    "name": None,
                                    "limit": 150,
                                    "offset": 0,
                                    "sort": [
                                        {
                                            "field": "timestamp",
                                            "order": "DESC"
                                        }
                                    ],
                                    "decorators": [],
                                    "type": "messages",
                                    "filter": None,
                                },
                            ],
                        },
                    ],
                    "parameters": [],
                }),
                headers=self.getAuthHeader(),
                timeout=30,
            )
            searchResponse.raise_for_status()

            response = request.post(
                graylogUrl(f'/api/views/search/{self.HEX_ID}/execute'),
                data=json.dumps({ 'parameter_bindings': {} }),
                headers=self.getAuthHeader(),
                timeout=30,
            )
            response.raise_for_status()

            return handleResponse(response)
        except requestExceptions.HTTPError as error:
            return handleResponse(response)
        except requestExceptions.Timeout as error:
            print(f'Истекло время ожидания запроса по событию: {queryString}')
        except Exception as error:
            print(f'Error to get viewsSearchExecute: {str(error)}')

        return None

    def viewsSearchExecuteValue(self, queryString, fromDate, toDate):
        try:
            response = self.viewsSearchExecute(queryString, fromDate, toDate)

            if response is None or response.get('data') is None:
                return None

            _results = response['data']['results']
            _results = _results[next(iter(_results))]
            _results = _results['search_types']
            _results = _results[next(iter(_results))]

            return _results['total_results']
        except Exception as error:
            print(f'Error to get viewsSearchExecuteValue: {str(error)}')

        return None
