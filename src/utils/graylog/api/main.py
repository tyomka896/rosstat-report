
def handleResponse(response):
    result = {}

    result['status'] = response.status_code

    if response.status_code < 400:
        result['data'] = response.json()
    else:
        result['error'] = response.json()

    return result
