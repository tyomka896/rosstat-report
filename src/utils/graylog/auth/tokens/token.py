import arrow

class Token():
    def __init__(self, value, time) -> None:
        if value is None or type(value) != str:
            raise Exception('Invalid parameter: value.')

        self._value = value

        if type(time) == int:
            self._expires_at = arrow.now().shift(seconds=time)
        elif type(time) == str:
            self._expires_at = arrow.get(time)
        else:
            raise Exception('Invalid parameter: time.')

    @property
    def value(self):
        return (
            self._value
            if not self._tokenHasExpired()
            else None
        )

    @property
    def expiresAt(self):
        return self._expires_at

    def _tokenHasExpired(self):
        return (
            (self._expires_at - arrow.utcnow()).total_seconds() <= 3
            if self._value is not None and self._expires_at is not None
            else True
        )



