import json
from urllib.parse import urlparse

from config.env import env, graylogUrl
from utils.request import request, requestExceptions
from utils.helpers import exitApp, encode64
from utils.graylog.auth.tokens.main import Tokens

class Auth(Tokens):
    def __init__(self):
        super().__init__()

        self._headers = {
            'Content-Type': 'application/json',
            'X-Requested-By': 'XMLHttpRequest',
            'X-Requested-With': 'XMLHttpRequest',
        }

    def getAuthHeader(self):
        _token = self.getAccessToken()

        if _token:
            _token64 = encode64(f'{_token}:session')

            return {
                'Authorization': f'Basic {_token64}',
                'Content-Type': 'application/json',
                'X-Requested-By': 'XMLHttpRequest',
                'X-Requested-With': 'XMLHttpRequest',
            }
        else:
            return None

    def getAccessToken(self):
        _token = self.accessToken

        if _token is not None:
            return _token

        self._requestAccessToken()

        return self.accessToken

    def _requestAccessToken(self):
        try:
            response = request.post(
                graylogUrl('/api/system/sessions'),
                data=json.dumps({
                    'host': urlparse(graylogUrl()).netloc,
                    'username': env.GRAYLOG_USERNAME,
                    'password': env.GRAYLOG_PASSWORD,
                }),
                headers=self._headers,
            )
            response.raise_for_status()

            responseJson = response.json()

            self.tokens({
                'access_token': responseJson['session_id'],
                'expires_at': responseJson['valid_until'],
            })
        except requestExceptions.HTTPError as error:
            print(f'FAIL Не удалось авторизоваться в Graylog с ошибкой: {error.response.json()}')

            exitApp(2)
        except requestExceptions.RequestException as error:
            print(f'Error to retrieve access token: {str(error)}')

            exitApp(2)
