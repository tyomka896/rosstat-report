from config.env import graylogUrl
from utils.request import request, requestExceptions
from utils.graylog.auth.main import Auth
from utils.graylog.api.metadata import Metadata

class __GraylogClass(Auth, Metadata):
    def __init__(self):
        Auth.__init__(self)
        Metadata.__init__(self)

    def checkConnection(self):
        try:
            response = request.get(graylogUrl(), timeout=15)
            response.raise_for_status()

            self.getAccessToken()

            if response.status_code == 200:
                return True
            else:
                return False
        except requestExceptions.HTTPError:
            return False
        except requestExceptions.ConnectTimeout:
            return False

Graylog = __GraylogClass()
