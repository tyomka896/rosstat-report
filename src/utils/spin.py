from yaspin import yaspin
from yaspin.spinners import Spinners

class __Spin():
    def __init__(self) -> None:
        self._spinner = None

    def start(self, text, **args):
        if self._spinner is not None:
            self.stop()

        spin = getattr(Spinners, 'line')

        self._spinner = yaspin(spin, text=text, **args)
        self._spinner.start()

        return self

    def stop(self, text = None):
        if self._spinner is not None:
            self.update(text)

            self._spinner.stop()

        self._spinner = None

        return self

    def fail(self, text = None):
        if self._spinner is not None:
            self.update(text)

            self._spinner.fail()

        return self.stop(text)

    def ok(self, text = None):
        if self._spinner is not None:
            self.update(text)

            self._spinner.ok()

        return self.stop(text)

    def update(self, text = None):
        if self._spinner is not None and text is not None:
            self._spinner.text = text

        return self

spin = __Spin()
