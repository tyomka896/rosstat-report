import sys
import math
import arrow
import base64
import traceback

from utils.spin import spin

def exitApp(code = 0):
    spin.stop()

    if code != 0:
        stacktrace()

    try:
        input('\nДля продолжения нажмите Enter. . .')
    except:
        pass

    sys.exit(code)

def isExe() -> bool:
    """
    Is application executable
    """
    return getattr(sys, 'frozen', False)

def stacktrace(force = False):
    if isExe() and not force:
        return

    print(traceback.format_exc())

def toBool(value, default = False):
    """
    Parse value to boolean
    """
    if type(value) == str and value.strip() != '':
        if value.lower() == 'true':
            return True
        else:
            try:
                return bool(int(value))
            except:
                return False
    elif type(value) == int:
        return bool(value)

    return default

def toInt(value, default = 0):
    """
    Parse value to integer
    """
    try:
        return int(value)
    except:
        return default

def dictValue(object, value, default = None):
    """
    Get key value from dictionary
    """
    if not type(object) == dict:
        raise Exception('Invalid parameter: object.')
    if not type(value) == str:
        raise Exception('Invalid parameter: value.')

    _object = object

    parts = value.split('.')

    for part in parts:
        if type(_object) == list:
            index = toInt(part, -1)

            if len(_object) > index >= 0:
                _object = _object[index]
            else:
                return default
        elif type(_object) == dict and part in _object:
            _object = _object[part]
        else:
            return default

    return _object

def monthName(daytime):
    if type(daytime) != arrow.arrow.Arrow:
        raise Exception('Invalid parameter: daytime.')

    MONTHS = [ 'январь', 'февраль', 'март', 'апрель', 'май', 'июнь', 'июль', 'август', 'сентябрь', 'октябрь', 'ноябрь', 'декабрь' ]

    return MONTHS[int(daytime.format('M')) - 1]

def convertSize(bytes):
    """
    Convert byte size to readable text
    """
    if bytes == 0:
        return '0 B'

    _size = 1024
    _size_names = ('B', 'KB', 'MB', 'GB', 'TB', 'PB')

    i = int(math.floor(math.log(bytes, _size)))
    s = round(bytes / math.pow(_size, i), 2)

    return "%s %s" % (s, _size_names[i])

def encode64(value):
    if type(value) != str:
        raise Exception('Invalid parameter: value.')

    return base64.b64encode(value.encode('utf-8')).decode('utf-8')
